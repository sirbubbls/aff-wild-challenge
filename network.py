import tensorflow as tf
from dataset import get_batch, get_batch_v2
from datetime import datetime
import sys

now = datetime.utcnow().strftime("Y%m%d%H%M%S")
logdir = 'logs/run-{}/'.format(now)

restore = 'false'
try:
    restore = sys.argv[2]
except Exception:
    pass

# Model Parameters

num_attributes = 2
width, height = (128, 128)

# Variables

X = tf.placeholder(tf.float32, shape=(None, width, height, 1), name='X')
y = tf.placeholder(tf.float32, shape=(None, num_attributes), name='y')

# DNN Layers

with tf.name_scope('Layer1'):
    conv1 = tf.layers.conv2d(
        X,
        filters=96,
        kernel_size=6,
        strides=[2, 2],
        padding='SAME',
        activation=tf.nn.relu
    )

with tf.name_scope('Layer2'):
    conv2 = tf.layers.conv2d(
        conv1,
        filters=256,
        kernel_size=4,
        strides=[2, 2],
        padding='SAME',
        activation=tf.nn.relu
    )

with tf.name_scope('Layer3'):
    conv3 = tf.layers.conv2d(
        conv2,
        filters=300,
        kernel_size=4,
        strides=[2, 2],
        padding='SAME',
        activation=tf.nn.relu
    )

with tf.name_scope('Layer4'):
    pool3 = tf.layers.max_pooling2d(
        conv3,
        2,
        2,
        padding='VALID'
    )

with tf.name_scope("FlattenDropoutLayer"):
    flatten = tf.layers.flatten(pool3)
    dropout = tf.layers.dropout(flatten, 0.1)

with tf.name_scope('DenseLayer'):
    dense1 = tf.layers.dense(dropout, 4096, activation=tf.nn.relu)
    dense2 = tf.layers.dense(dense1, 2048, activation=tf.nn.relu)

with tf.name_scope("OutputLayer"):
    logits = tf.layers.dense(dense2, num_attributes, activation=tf.nn.relu)


# Loss and Train Function

with tf.name_scope('loss'):
    loss = tf.losses.mean_squared_error(y, logits)

with tf.name_scope('train'):
    optimizer = tf.train.AdamOptimizer(0.001)
    training_op = optimizer.minimize(loss)


init = tf.global_variables_initializer()

n_epochs = int(sys.argv[1])
step = 0

saver = tf.train.Saver()
file_writer = tf.summary.FileWriter(logdir, tf.get_default_graph())
loss_summary = tf.summary.scalar('Loss', loss)

with tf.Session() as sess:
    if restore == 'true':
        try:
            saver.restore(sess, './model/model1.ckpt')
            print("Model Loaded ...")
        except Exception:
            init.run()
    else:
        init.run()

    for epoch in range(n_epochs):
        X_batch, y_batch = get_batch_v2(2000)
        sess.run(training_op, feed_dict={X: X_batch, y: y_batch})
        eval_loss = loss.eval(feed_dict={X: X_batch, y: y_batch})

        write_summary = loss_summary.eval(feed_dict={X: X_batch, y: y_batch})
        step += X_batch.shape[0]
        file_writer.add_summary(write_summary, step)
        print('Epoch {}:\nLoss: {}'.format(epoch, eval_loss))
        saver.save(sess, './model/model1.ckpt')
