# Project
Dataset: [Link](https://ibug.doc.ic.ac.uk/resources/first-affect-wild-challenge/)

Attributes of the Dataset:
- Valence
- Arousal

## Dataset Folder Structure
rssources/{boxes,labels, videos}/train
