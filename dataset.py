import cv2 as cv
from time import sleep
import os
from numpy.random import randint
from numpy import array
import numpy as np

# Get Video/Labels
X_train_video_path = './ressources/videos/train/'
X_train_boxes = './ressources/boxes/train/'
y_train = './ressources/labels/train/'

target_width = 128
target_height = 128

# Video Files
train_videos = os.listdir(X_train_video_path)


def get_box(name, frame):
    name = name.split('.')[0]
    path = X_train_boxes + name + '/'

    path += str(frame) + '.pts'

    points = []

    with open(path) as fs:
        for cnt, line in enumerate(fs):
            if cnt >= 3 and cnt < 7:
                p = line.split(' ')
                points.append((int(float(p[0])), int(float(p[1]))))

    return points


def get_label(name, frame):
    path = name.split('.')[0] + '.txt'

    with open(y_train + 'arousal/' + path) as fs:
        for cnt, line in enumerate(fs):
            if cnt == frame:
                arousal = float(line)
                break

    with open(y_train + 'valence/' + path) as fs:
        for cnt, line in enumerate(fs):
            if cnt == frame:
                valence = float(line)
                break

    return arousal, valence


def get_labels(name):
    name = name.split('.')[0] + '.txt'

    arousal = []
    valence = []

    with open(y_train + 'arousal/' + name) as fs:
        for cnt, line in enumerate(fs):
            arousal.append(float(line))

        fs.close()

    with open(y_train + 'valence/' + name) as fs:
        for cnt, line in enumerate(fs):
            valence.append(float(line))

        fs.close()

    # Combining into Numpy Array
    return array([arousal, valence])


def get_batch(reduce_factor):
    # Select Video File
    target_video = train_videos[randint(len(train_videos))]
    print('Loading Video: {}'.format(target_video))
    video = cv.VideoCapture(X_train_video_path + target_video)

    frame_count = 0
    video_frames = []
    labels = []

    # Extracting Video Frames
    while video.isOpened():
        success, frame = video.read()

        if success and frame_count % reduce_factor == 0:
            # Calculating Frame
            try:
                box_points = get_box(target_video, frame_count)
            except:
                break
            height = box_points[1][1] - box_points[0][1]
            x, y = box_points[0]

            # Crop Frame
            cropped = frame[y:y + height, x:x + height]

            # Scale Image
            scaled = cv.resize(
                cropped, (target_width, target_height), interpolation=cv.INTER_AREA)

            # Converting to Grayscale
            scaled = cv.cvtColor(scaled, cv.COLOR_BGR2GRAY)

            # Getting Label Information
            valence, arousal = get_label(target_video, frame_count)
            labels.append([valence, arousal])

            # Adding to List
            video_frames.append(scaled)
            # print("Frame {}\tShape {}".format(frame_count, scaled.shape))
        elif success == False:
            break

        frame_count += 1

        if frame_count % 300 == 0:
            try:
                sample = video_frames[np.random.randint(len(video_frames))]
                cv.imshow('Sample Image', sample)
                if cv.waitKey(1) == 27:
                    pass
            except:
                pass
    # Reshaping Image Data
    X = np.array(video_frames).reshape(-1, target_width,
                                       target_height, 1).astype(np.float32)
    y = np.array(labels)

    # Returning Data
    print('Image Data Shape: {}\nLabel Shape: {}'.format(X.shape, y.shape))
    print('Total Frames: {}'.format(frame_count))
    print('')

    if X.shape[0] == 0:
        return get_batch(reduce_factor)
    return X, y * 10


def get_batch_v2(size):
    X, y = get_batch(10)

    for i in range(10):
        X_batch, y_batch = get_batch(5)

        # Concatenate
        X = np.append(X, X_batch, axis=0)
        y = np.append(y, y_batch, axis=0)

    # Randomizing
    rand = np.random.permutation(y.shape[0])

    print(y[:10])
    print(X.shape)

    X = X[rand]
    y = y[rand]
    try:
        return X[:size], y[:size]
    except Exception:
        return X, y

# X, y = get_batch_v2(20)

# print(X.shape)
# print(y.shape)

# print(get_box('140', 1))
